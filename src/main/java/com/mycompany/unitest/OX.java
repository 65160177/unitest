/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;

/**
 *
 * @author Asus
 */
class OX {

    static boolean checkWin(String[][] table, String player) {
        
        if (checkRow(table, player)) {
            return true;
        }else if (checkCol(table, player)){
            return true;
        }else if (checkDi(table, player)){
            return true;
        }else if (checkDi(table, player)){
            return true;
        }
        
        if(checkDraw(table, player)){
            return true;
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String player) {
        for (int row = 0; row<3; row++) {
            if (checkRow(table, player, row)) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkCol(String[][] table, String player) {
        for (int col = 0; col<3; col++) {
            if (checkCol(table, player, col)) {
                return true;
            }
        }
        return false;
    }
    

    private static boolean checkRow(String[][] table, String player,int row) {
        return table[row][0].equals(player) && table[row][1].equals(player) && table[row][2].equals(player);
    }

    private static boolean checkCol(String[][] table, String player, int col) {
        return table[0][col].equals(player) && table[1][col].equals(player) && table[2][col].equals(player);
    }

    private static boolean checkDi(String[][] table, String player) {
        if (table[0][0].equals(player) && table[1][1].equals(player) && table[2][2].equals(player)){
            return true;
        }else if (table[0][2].equals(player) && table[1][1].equals(player) && table[2][0].equals(player)){
            return true;
        }
        return false;
    }

    

    private static boolean checkDraw(String[][] table, String player) {
        
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                if (table[i][j].equals(player)){
                    int c=0;
                    c++;
                    if(i==8){
                        return true;
                    }
                    
                }
            }
            
        }
        return false;
    }

}
